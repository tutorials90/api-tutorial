﻿using DB;
using Soko.Server.DTO.Base;

namespace DTO.Change
{
    public class PostChangeDTO : InputBaseDto
    {
        public Post ChangeInfo { get; set; }

        public bool Equals(PostChangeDTO other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(ChangeInfo, other.ChangeInfo);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PostChangeDTO)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = ChangeInfo != null ? ChangeInfo.GetHashCode() : 0;
                return hashCode;
            }
        }
    }
}
