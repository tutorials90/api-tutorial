﻿using Soko.Server.DTO.Base;

namespace DTO.Update
{
    public class PostUpdateDTO : InputBaseDto
    {
        public DB.Post ChangeInfo { get; set; }

        public bool Equals(BlogUpdateDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(ChangeInfo, other.ChangeInfo);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((BlogUpdateDto)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = ChangeInfo != null ? ChangeInfo.GetHashCode() : 0;
                return hashCode;
            }
        }
    }
}
