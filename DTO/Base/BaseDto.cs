﻿using Newtonsoft.Json.Linq;

namespace Soko.Server.DTO.Base
{
    public abstract class BaseDto
    {
        public abstract override bool Equals(object obj);
        public abstract override int GetHashCode();
    }
}
