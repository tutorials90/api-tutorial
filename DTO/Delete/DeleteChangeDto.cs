﻿using Soko.Server.DTO.Base;
using Soko.Server.DTO.Common;

namespace DTO.Delete
{
    public class DeleteChangeDto : InputBaseDto
    {
        public DeleteInfo DeleteInfo { get; set; }

        public bool Equals(DeleteChangeDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(DeleteInfo, other.DeleteInfo);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((DeleteChangeDto)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = DeleteInfo != null ? DeleteInfo.GetHashCode() : 0;
                return hashCode;
            }
        }
    }
}
