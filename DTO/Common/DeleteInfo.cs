﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Soko.Server.DTO.Common
{
    public class DeleteInfo
    {
        [Required]
        public Guid Id { get; set; }

        public bool Equals(DeleteInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(Id, other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((DeleteInfo)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return Id != null ? Id.GetHashCode() : 0;
            }
        }
    }
}
