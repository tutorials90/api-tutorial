﻿using BL;
using DB;
using DTO.Change;
using DTO.Delete;
using DTO.Update;
using Microsoft.AspNetCore.Mvc;

namespace api_tutorial.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/Post/[action]")]
    public class PostController : Controller
    {
        private readonly ILogger<PostController> _logger;
        public PostController(ILogger<PostController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            _logger.LogDebug("Post.GetPosts started"); try
            {
                var output = await PostBL.GetPostAsync(_logger);
                _logger.LogDebug("Post.GetPosts finished");
                return new ObjectResult(new { posts = output, error = (string)null });
            }
            catch (Exception ex)
            {
                _logger.LogDebug("Post.GetPosts finished");
                return new ObjectResult(new { posts = new List<Post>(), error = ex.ToString() }) { StatusCode = StatusCodes.Status500InternalServerError };
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] PostChangeDTO input)
        {
            _logger.LogDebug("Post.CreatePost started");
            try
            {
                var output = await PostBL.CreatePostAsync(input, _logger);
                _logger.LogDebug("Post.CreatePost finished");
                return new ObjectResult(new { response = output, error = (string)null });
            }
            catch (Exception ex)
            {
                _logger.LogDebug("Post.CreatePost finished");
                return new ObjectResult(new { response = (string)null, error = ex.ToString() }) { StatusCode = StatusCodes.Status500InternalServerError };
            }
        }

        [HttpPatch]
        public async Task<IActionResult> Update([FromBody] PostUpdateDTO input)
        {
            _logger.LogDebug("Post.UpdatePost started");
            try
            {
                var output = await PostBL.UpdatePostAsync(input, _logger);
                _logger.LogDebug("Post.UpdatePost finished");
                return new ObjectResult(new { response = output, error = (string)null });
            }
            catch (Exception ex)
            {
                _logger.LogDebug("Post.UpdatePost finished");
                return new ObjectResult(new { response = (string)null, error = ex.ToString() }) { StatusCode = StatusCodes.Status500InternalServerError };
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] DeleteChangeDto input)
        {
            _logger.LogDebug("Post.DeletePost started");
            try
            {
                var output = await PostBL.DeletePostAsync(input, _logger);
                _logger.LogDebug("Post.DeletePost finished");
                return new ObjectResult(new { response = output, error = (string)null });
            }
            catch (Exception ex)
            {
                _logger.LogDebug("Post.DeletePost finished");
                return new ObjectResult(new { response = (string)null, error = ex.ToString() }) { StatusCode = StatusCodes.Status500InternalServerError };
            }
        }
    }
}
