﻿using BL;
using DB;
using DTO.Change;
using DTO.Delete;
using DTO.Update;
using Microsoft.AspNetCore.Mvc;

namespace api_tutorial.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/Blog/[action]")]
    public class BlogController : Controller
    {
        private readonly ILogger<BlogController> _logger;
        public BlogController(ILogger<BlogController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            _logger.LogDebug("Blog.GetBlogs started"); try
            {
                var output = await BlogBL.GetBlogAsync(_logger);
                _logger.LogDebug("Blog.GetBlogs finished");
                return new ObjectResult(new { posts = output, error = (string)null });
            }
            catch (Exception ex)
            {
                _logger.LogDebug("Blog.GetBlogs finished");
                return new ObjectResult(new { posts = new List<Blog>(), error = ex.ToString() }) { StatusCode = StatusCodes.Status500InternalServerError };
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] BlogChangeDto input)
        {
            _logger.LogDebug("Blog.CreateBlog started");
            try
            {
                var output = await BlogBL.CreateBlogAsync(input, _logger);
                _logger.LogDebug("Blog.CreateBlog finished");
                return new ObjectResult(new { response = output, error = (string)null });
            }
            catch (Exception ex)
            {
                _logger.LogDebug("Blog.CreateBlog finished");
                return new ObjectResult(new { response = (string)null, error = ex.ToString() }) { StatusCode = StatusCodes.Status500InternalServerError };
            }
        }

        [HttpPatch]
        public async Task<IActionResult> Update([FromBody] BlogUpdateDto input)
        {
            _logger.LogDebug("Blog.UpdateBlog started");
            try
            {
                var output = await BlogBL.UpdateBlogAsync(input, _logger);
                _logger.LogDebug("Blog.UpdateBlog finished");
                return new ObjectResult(new { response = output, error = (string)null });
            }
            catch (Exception ex)
            {
                _logger.LogDebug("Blog.UpdateBlog finished");
                return new ObjectResult(new { response = (string)null, error = ex.ToString() }) { StatusCode = StatusCodes.Status500InternalServerError };
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] DeleteChangeDto input)
        {
            _logger.LogDebug("Blog.DeleteBlog started");
            try
            {
                var output = await BlogBL.DeleteBlogAsync(input, _logger);
                _logger.LogDebug("Blog.DeleteBlog finished");
                return new ObjectResult(new { response = output, error = (string)null });
            }
            catch (Exception ex)
            {
                _logger.LogDebug("Blog.DeleteBlog finished");
                return new ObjectResult(new { response = (string)null, error = ex.ToString() }) { StatusCode = StatusCodes.Status500InternalServerError };
            }
        }
    }
}
