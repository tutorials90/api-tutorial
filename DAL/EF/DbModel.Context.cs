﻿using DAL.EF.EntityTables;
using Microsoft.EntityFrameworkCore;

namespace DAL.EF
{
    public class DbModelContainer : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // use PostgreSchema variable as default schema name
            optionsBuilder.UseNpgsql(Environment.GetEnvironmentVariable("PostgreConnectionString"));
            //optionsBuilder.UseNpgsql("Host=127.0.0.1;Username=postgres;Password=1111;Database=tutorial_db;Port=5432");

            optionsBuilder.UseLazyLoadingProxies();
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("dbo");

            modelBuilder.Entity<Post>()
                .HasOne(p => p.Blog)
                .WithMany(b => b.Posts);

        }

        public virtual DbSet<Blog> Blogs { get; set; }
        public async Task<Guid?> AddBlogAsync(Blog entity)
        {
            Guid? output = null;
            if (entity != null)
            {
                await Blogs.AddAsync(entity);
                output = entity.Id;
                await SaveChangesAsync();

            }
            return output;
        }

        public async Task<Guid?> UpdateBlogAsync(Blog entity)
        {
            Guid? output = null;
            if (entity != null)
            {
                Blogs.Update(entity);
                output = entity.Id;
                await SaveChangesAsync();
            }
            return output;
        }

        public async Task<Guid?> DeleteBlogAsync(Blog entity)
        {
            Guid? output = null;
            if (entity != null)
            {
                Blogs.Remove(entity);
                output = entity.Id;
                await SaveChangesAsync();
            }
            return output;
        } 
        
        public async Task<Guid?> DeleteBlogAsync(Guid id)
        {
            Guid? output = null;
            var entity = await Blogs.FirstOrDefaultAsync(it => it.Id == id);
            if (entity != null)
            {
                Blogs.Remove(entity);
                output = entity.Id;
                await SaveChangesAsync();
            }
            return output;
        }


        public virtual DbSet<Post> Posts { get; set; }
        public async Task<Guid?> AddPostAsync(Post entity)
        {
            Guid? output = null;
            if (entity != null)
            {
                await Posts.AddAsync(entity);
                output = entity.Id;
                await SaveChangesAsync();

            }
            return output;
        }

        public async Task<Guid?> UpdatePostAsync(Post entity)
        {
            Guid? output = null;
            if (entity != null)
            {
                Posts.Update(entity);
                output = entity.Id;
                await SaveChangesAsync();
            }
            return output;
        }

        public async Task<Guid?> DeletePostAsync(Post entity)
        {
            Guid? output = null;
            if (entity != null)
            {
                Posts.Remove(entity);
                output = entity.Id;
                await SaveChangesAsync();
            }
            return output;
        }

        public async Task<Guid?> DeletePostAsync(Guid id)
        {
            Guid? output = null;
            var entity = await Posts.FirstOrDefaultAsync(it => it.Id == id);
            if (entity != null)
            {
                Posts.Remove(entity);
                output = entity.Id;
                await SaveChangesAsync();
            }
            return output;
        }
    }
}
