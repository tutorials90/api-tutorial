﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.EF.EntityTables
{
    public class Blog
    {
        public Guid Id { get; set; }
        public string Url { get; set; }

        public virtual List<Post> Posts { get; set; }
    }

}
