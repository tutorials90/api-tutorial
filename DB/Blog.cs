﻿namespace DB
{
    public class Blog
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
    }
}
