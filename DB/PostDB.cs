﻿using DAL.EF;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB
{
    public class PostDB
    {
        public static async Task<List<Post>> GetPostsAsync()
        {
            using (var context = new DbModelContainer())
            {
                return await context.Posts.Select(p => new Post() { Id = p.Id, Title = p.Title, Content = p.Content, BlogId = p.BlogId }).ToListAsync();
            }
        }

        public static async Task<Guid?> DeletePostAsync(Guid id)
        {
            using (var context = new DbModelContainer())
            {
                Guid? dbOutput = await context.DeletePostAsync(id);
                return dbOutput;
            }
        }

        public static async Task<Guid?> CreatePostAsync(Post input)
        {
            DAL.EF.EntityTables.Post mappedInput = new DAL.EF.EntityTables.Post();
            mappedInput.Id = input.Id;
            mappedInput.Title = input.Title;
            mappedInput.Content = input.Content;
            mappedInput.BlogId = input.BlogId;

            using (var context = new DbModelContainer())
            {
                Guid? dbOutput = await context.AddPostAsync(mappedInput);
                return dbOutput;
            }
        }

        public static async Task<Guid?> UpdatePostAsync(Post changeInfo)
        {
            Guid? dbOutput = null;
            using (var context = new DbModelContainer())
            {
                if (changeInfo != null)
                {
                    var objectToUpdate = await context.Posts.FindAsync(changeInfo.Id);
                    if (objectToUpdate != null)
                    {
                        objectToUpdate.Title = changeInfo.Title;
                        objectToUpdate.Content = changeInfo.Content;
                        objectToUpdate.BlogId = changeInfo.BlogId;
                        dbOutput = await context.UpdatePostAsync(objectToUpdate);
                    }
                    else throw new Exception("Object with given Id not found");
                }
            }
            return dbOutput;
        }
    }
}
