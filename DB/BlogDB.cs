﻿using DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace DB
{
    public class BlogDB
    {
        public static async Task<List<Blog>> GetBlogsAsync()
        {
            using (var context = new DbModelContainer())
            {
                return await context.Blogs.Select(b => new Blog() { Id = b.Id, Url = b.Url }).ToListAsync();
            }
        }

        public static async Task<Guid?> DeleteBlogAsync(Guid id)
        {
            using (var context = new DbModelContainer())
            {
                Guid? dbOutput = await context.DeleteBlogAsync(id);
                return dbOutput;
            }
        }

        public static async Task<Guid?> CreateBlogAsync(Blog input)
        {
            DAL.EF.EntityTables.Blog mappedInput = new DAL.EF.EntityTables.Blog();
            mappedInput.Id = input.Id;
            mappedInput.Url = input.Url;

            using (var context = new DbModelContainer())
            {
                Guid? dbOutput = await context.AddBlogAsync(mappedInput);
                return dbOutput;
            }
        }

        public static async Task<Guid?> UpdateBlogAsync(Blog changeInfo)
        {
            Guid? dbOutput = null;
            using (var context = new DbModelContainer())
            {
                if (changeInfo != null)
                {
                    var objectToUpdate = await context.Blogs.FindAsync(changeInfo.Id);
                    if (objectToUpdate != null)
                    {
                        objectToUpdate.Url = changeInfo.Url;
                        dbOutput = await context.UpdateBlogAsync(objectToUpdate);
                    }
                    else throw new Exception("Object with given Id not found");
                }
            }
            return dbOutput;
        }
    }
}
