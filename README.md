# API Tutorial

Príklad API v .NET 6.0.


# Štruktúra Projectu

WS - základná aplikácia (Startup Projeckt)
BL - Business Logic Layer - vrstva na ktorej je logika aplikácie ako napríklad validácia vstupných údajov, overenie práv používatela atď.
DB - databázová vrstva - dodatočné spracovanie dát (ak je potreba)
DAL - Data Access Layer - vrstva ktorá slúži na prístup k dátam uložených v databáze. Tu používame Entity Framework.
DTO - Data transfer object - mapovanie vstupných dát interné objekty, mapovanie interných objecktov na výstupné dáta

## WS

Obsahuje Controller-i:

 - BlogController 
 - PostController

Každý Controller obsahuje metódy: 
- Get (HttpGet)- vyberie dáta z databázy a vráti ich ako response
- Create (HttpPost)- vytvorenie nového objektu
- Update (HttpPatch)- úprava objektu
- Delete (HttpDelete)- zmazanie objektu

## BL
V tomto príklade iba prevoláva metódy z vyššej vrstvy. V reálnej aplikácii sa tu overuje či má užívateľ práva na použitie metódy a či sú vstupné dáta kompletné.

## DB
V tomto príklade iba prevoláva metódy z vyššej vrstvy. V reálnej aplikácii sa tu napríklad doplňujú vstupné dáta o defaultné hodnoty. 

## DAL
Tu sa definuje databázový model pomocou ORM. ORM - Object-Relational Mapping - slúži na manipuláciu dát v databáze formou objektov - mapuje databázové tabuľky na objekty pre uľahčenie práce s nimi. V našom prípade to je Entity Framework. 
Náš databázový model obsahuje 2 tabuľky s One-to-many závislosťou Blog-Post : jeden Blog má viacero Postov, Posty majú iba jeden Blog.

Pozrieť tiež:
One-to-one : napr. Človek-OP: jeden človek má jeden občiansky preukaz, jeden občiansky preukaz má jedného človeka.

Many-to-many : napr. Žiak-Predmet - veľa žiakov má matematiku, na matematiku chodí viacero žiakov.,

Naša databáza je tvorená ako Code first - najprv sme si predpísali Entity (objekty) a nechali ORM aby z toho vygenerovalo tabuľky. Tabuľky sa generujú cez tzv. migrácie. 
Pozri:  https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli
Migrácie obsahujú zoznam zmien v databázovom modeli od predchádzajúcej verzie.

Ako databázu používame Postgres. Pri štarte aplikácie sa automaticky aplikujú všetky migrácie.
Connection string na databázu určuje premenná prostredia PostgreConnectionString.
Nacádza sa v: api-tutorial\WS\Properties\launchSettings.json
