﻿using DB;
using DTO.Change;
using DTO.Delete;
using DTO.Update;
using Microsoft.Extensions.Logging;

namespace BL
{
    public class PostBL
    {
        public static async Task<List<Post>> GetPostAsync(ILogger _log)
        {
            return await PostDB.GetPostsAsync();
        }

        public static async Task<Guid?> CreatePostAsync(PostChangeDTO input, ILogger _log)
        {
            Guid? output = await PostDB.CreatePostAsync(input.ChangeInfo);
            return output;
        }

        public static async Task<Guid?> UpdatePostAsync(PostUpdateDTO input, ILogger _log)
        {
            Guid? output = await PostDB.UpdatePostAsync(input.ChangeInfo);
            return output;
        }

        public static async Task<Guid?> DeletePostAsync(DeleteChangeDto input, ILogger _log)
        {
            Guid? output = await PostDB.DeletePostAsync(input.DeleteInfo.Id);
            return output;
        }
    }
}
