﻿using DB;
using DTO.Change;
using DTO.Delete;
using DTO.Update;
using Microsoft.Extensions.Logging;

namespace BL
{
    public class BlogBL
    {
        public static async Task<List<Blog>> GetBlogAsync(ILogger _log)
        {
            return await BlogDB.GetBlogsAsync();
        }

        public static async Task<Guid?> CreateBlogAsync(BlogChangeDto input, ILogger _log)
        {
            Guid? output = await BlogDB.CreateBlogAsync(input.ChangeInfo);
            return output;
        }

        public static async Task<Guid?> UpdateBlogAsync(BlogUpdateDto input, ILogger _log)
        {
            Guid? output = await BlogDB.UpdateBlogAsync(input.ChangeInfo);
            return output;
        }

        public static async Task<Guid?> DeleteBlogAsync(DeleteChangeDto input, ILogger _log)
        {
            Guid? output = await BlogDB.DeleteBlogAsync(input.DeleteInfo.Id);
            return output;
        }
    }
}
